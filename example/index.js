import React from 'react'
import dva, {
  createRouter
} from 'react-native-dva';
import models from './src/models';
import router from './src/router';
import {
  name
} from './app.json';

const {
  routerMiddleware,
  routerReducer,
  Router
} = createRouter(router);

const app = dva({
  models,
  extraReducers: { router: routerReducer },
  onAction: [routerMiddleware]
});

app.start(name, <Router />);
