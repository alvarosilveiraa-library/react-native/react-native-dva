import {
  Home
} from './screens';

export default {
  type: 'stack',
  routes: [
    {
      type: 'tab-bottom',
      name: 'TabBottom',
      routes: [
        {
          type: 'screen',
          name: 'Home',
          label: 'Home',
          icon: '',
          component: Home
        },
        {
          type: 'screen',
          name: 'Home2',
          label: 'Home2',
          icon: '',
          component: Home
        },
        {
          type: 'screen',
          name: 'Home3',
          label: 'Home3',
          icon: '',
          component: Home
        },
        {
          type: 'screen',
          name: 'Home4',
          label: 'Home4',
          icon: '',
          component: Home
        }
      ],
      options: {
        style: {
          height: 56,
          borderTopColor: '#000000'
        },
        activeTintColor: '#ffffff',
        inactiveTintColor: '#000000'
      }
    },
    {
      type: 'screen',
      name: 'HomeStack',
      component: Home
    }
  ]
}
