import React, { Component } from 'react';
import { StyleSheet, View, Button, Text } from 'react-native';
import { connect } from 'react-redux';

class Home extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>{this.props.navigation.state.routeName}</Text>

        <Button
          onPress={() => this.props.navigation.navigate('HomeStack')}
          title="Learn More"
          color="#841584"
          accessibilityLabel="Learn more about this purple button"
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 32,
    height: 32,
  },
})

export default connect(({ user }) => ({ user }))(Home);
