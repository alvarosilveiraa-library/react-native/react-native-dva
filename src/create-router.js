import {
  createReactNavigationReduxMiddleware,
  createNavigationReducer
} from 'react-navigation-redux-helpers';
import createRouter from './Router';
import createNavigation from './create-navigation';

export default router => {
  const Navigation = createNavigation(router);

  const routerReducer = createNavigationReducer(Navigation);

  const routerMiddleware = createReactNavigationReduxMiddleware(
    // 'root',
    state => state.router
  );

  return {
    routerReducer,
    routerMiddleware,
    Router: createRouter(Navigation)
  }
}
