import React from 'react';
import {
  AppRegistry
} from 'react-native';
import {
  create
} from 'dva-core';
import createLoading from 'dva-loading';
import {
  Provider
} from 'react-redux';
import {
  persistStore,
  autoRehydrate
} from 'redux-persist';

export default function(options) {
  options.models = options.models || [];

  options.extraEnhancers = options.extraEnhancers || [];

  if(options.persist) options.extraEnhancers.push(autoRehydrate());

  const app = create(options);

  app.use(createLoading());

  if(
    options.models
    && Array.isArray(options.models)
    && options.models.length
  ) options.models.forEach(model => app.model(model));

  app.start();

  if(options.persist) persistStore(app._store);

  const store = app._store;

  app.start = (name, children) => {
    const App = () => (
      <Provider store={store}>
        {children}
      </Provider>
    );

    AppRegistry.registerComponent(name, () => App);
  }

  app.getStore = () => store;

  return app;
}
