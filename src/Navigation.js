import React from 'react';

export default (Navigator, Layout) => {
  class Navigation extends React.Component {
    static router = Navigator.router;

    render() {
      const { navigation } = this.props;

      return (
        <Layout navigation={navigation}>
          <Navigator navigation={navigation} />
        </Layout>
      );
    }
  }

  return Navigation;
}
