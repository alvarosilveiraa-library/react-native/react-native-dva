import React from 'react';
import {
  BackHandler
} from 'react-native';
import {
  NavigationActions
} from 'react-navigation';
import {
  createReduxContainer
} from 'react-navigation-redux-helpers';
import {
  connect
} from 'react-redux';

export default (Navigation, key = 'root') => {
  const App = createReduxContainer(Navigation, key);

  class Router extends React.PureComponent {
    componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', this.backHandle);
    }

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.backHandle);
    }

    backHandle = () => {
      return this.props.dispatch(NavigationActions.back());
    };

    render() {
      const {
        dispatch,
        router
      } = this.props;

      return <App dispatch={dispatch} state={router} />;
    }
  }

  return connect(({ router }) => ({ router }))(Router);
}
