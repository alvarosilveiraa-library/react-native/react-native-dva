import React from 'react';
import {
  Text
} from 'react-native';
import {
  createSwitchNavigator,
  createStackNavigator,
  createMaterialTopTabNavigator,
  createBottomTabNavigator,
  createDrawerNavigator
} from 'react-navigation';
import {
  createFluidNavigator
} from 'react-navigation-fluid-transitions';
import createNavigation from './Navigation';

const createScreen = ({ component, layout }) => {
  if(layout) return createNavigation(component, layout);

  return component;
}

const createTabTop = ({ routes, layout, options = {} }) => {
  if(!routes || !routes.length) return null;

  const screens = {};

  routes.forEach(route => {
    switch(route.type) {
      case 'screen':
        screens[route.name] = {
          screen: route.component,
          navigationOptions: {
            tabBarLabel: route.label
          }
        }

        break;
      default:
        break;
    }
  });

  const Navigator = createMaterialTopTabNavigator(screens, {
    initialRouteName: routes[0].name,
    ...options
  });

  if(layout) return createNavigation(Navigator, layout);

  return Navigator;
}

const createTabBottom = ({ routes, layout, options = {} }) => {
  if(!routes || !routes.length) return null;

  const screens = {};

  routes.forEach(route => {
    const tabBarIcon = ({ tintColor }) => route.icon(tintColor);

    const tabBarLabel = typeof route.label === 'string'
      ? ({ tintColor }) => (
        <Text
          style={{
            color: tintColor,
            fontSize: 12,
            textAlign: 'center',
            marginBottom: 8
          }}
        >
          {route.label}
        </Text>
      ) : ({ tintColor }) => route.label(tintColor);

    const navigationOptions = { tabBarIcon, tabBarLabel };

    switch(route.type) {
      case 'drawer':
        screens[route.name] = {
          screen: createDrawer(route),
          navigationOptions: route.navigationOptions ? route.navigationOptions : navigationOptions
        }

        break;
      case 'tab-top':
        screens[route.name] = {
          screen: createTabTop(route),
          navigationOptions: route.navigationOptions ? route.navigationOptions : navigationOptions
        }

        break;
      case 'screen':
        screens[route.name] = {
          screen: route.component,
          navigationOptions: route.navigationOptions ? route.navigationOptions : navigationOptions
        }

        break;
      default:
        break;
    }
  });

  const Navigator = createBottomTabNavigator(screens, {
    initialRouteName: routes[0].name,
    ...options,
    tabBarComponent: options.tabBarComponent
      ? props => (
        <options.tabBarComponent
          {...props}
          routes={routes}
          options={options.tabBarOptions}
        />
      ) : undefined
  });

  if(layout) return createNavigation(Navigator, layout);

  return Navigator;
}

const createDrawer = ({ routes, layout, options = {} }) => {
  if(!routes || !routes.length) return null;

  const screens = {};

  routes.forEach(route => {
    switch(route.type) {
      case 'tab-bottom':
        screens[route.name] = { screen: createTabBottom(route) };

        break;
      case 'tab-top':
        screens[route.name] = { screen: createTabTop(route) };

        break;
      case 'screen':
        screens[route.name] = { screen: route.component };

        break;
    }
  });

  const Navigator = createDrawerNavigator(screens, {
    initialRouteName: routes[0].name,
    ...options
  });

  if(layout) return createNavigation(Navigator, layout);

  return Navigator;
}

const createFluid = ({ routes, layout, options = {} }) => {
  if(!routes || !routes.length) return null;

  const screens = {};

  routes.forEach(route => {
    switch(route.type) {
      case 'fluid':
          screens[route.name] = { screen: createFluid(route) };

          break;
      case 'stack':
        screens[route.name] = { screen: createStack(route) };

        break;
      case 'drawer':
        screens[route.name] = { screen: createDrawer(route) };

        break;
      case 'tab-bottom':
        screens[route.name] = { screen: createTabBottom(route) };

        break;
      case 'tab-top':
        screens[route.name] = { screen: createTabTop(route) };

        break;
      case 'screen':
        screens[route.name] = { screen: createScreen(route) };

        break;
      default:
        break;
    }
  });

  const Navigator = createFluidNavigator(screens, {
    initialRouteName: routes[0].name,
    headerMode: 'none',
    ...options
  });

  if(layout) return createNavigation(Navigator, layout);

  return Navigator;
}

const createStack = ({ routes, layout, options = {} }) => {
  if(!routes || !routes.length) return null;

  const screens = {};

  routes.forEach(route => {
    switch(route.type) {
      case 'fluid':
          screens[route.name] = { screen: createFluid(route) };

          break;
      case 'stack':
        screens[route.name] = { screen: createStack(route) };

        break;
      case 'drawer':
        screens[route.name] = { screen: createDrawer(route) };

        break;
      case 'tab-bottom':
        screens[route.name] = { screen: createTabBottom(route) };

        break;
      case 'tab-top':
        screens[route.name] = { screen: createTabTop(route) };

        break;
      case 'screen':
        screens[route.name] = { screen: createScreen(route) };

        break;
      default:
        break;
    }
  });

  const Navigator = createStackNavigator(screens, {
    initialRouteName: routes[0].name,
    headerMode: 'none',
    ...options
  });

  if(layout) return createNavigation(Navigator, layout);

  return Navigator;
}

const createSwitch = ({ routes, layout, options = {} }) => {
  if(!routes || !routes.length) return null;

  const screens = {};

  routes.forEach(route => {
    switch(route.type) {
      case 'fluid':
        screens[route.name] = { screen: createFluid(route) };

        break;
      case 'stack':
        screens[route.name] = { screen: createStack(route) };

        break;
      case 'drawer':
        screens[route.name] = { screen: createDrawer(route) };

        break;
      case 'tab-bottom':
        screens[route.name] = { screen: createTabBottom(route) };

        break;
      case 'tab-top':
        screens[route.name] = { screen: createTabTop(route) };

        break;
      case 'screen':
        screens[route.name] = { screen: createScreen(route) };

        break;
      default:
        break;
    }
  });

  const Navigator = createSwitchNavigator(screens, {
    initialRouteName: routes[0].name,
    backBehavior: 'none',
    ...options
  });

  if(layout) return createNavigation(Navigator, layout);

  return Navigator;
}

export default router => {
  switch(router.type) {
    case 'switch':
      return createSwitch(router);
    case 'stack':
      return createStack(router);
    case 'fluid':
      return createFluid(router);
    case 'drawer':
      return createDrawer(router);
    case 'tab-bottom':
      return createTabBottom(router);
    case 'tab-top':
      return createTabTop(router);
    default:
      return null;
  }
}
