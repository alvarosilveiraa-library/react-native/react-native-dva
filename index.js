import dva from './src';
import {
  connect
} from 'react-redux';
import createRouter from './src/create-router.js';

export {
  connect,
  createRouter
}

export default dva;
